mojo_sass Extension
===================

This TYPO3 extension brings the power of Sass to the world of TYPO3. It is based on the
excellent PhamlP PHP port of Haml and Sass released under the new BSD license.

Why?
====

Sass makes CSS fun again. Sass is an extension of CSS3, adding nested
rules, variables, mixins, selector inheritance, and more. It’s translated
to well-formatted, standard CSS using the command line tool or a
web-framework plugin.

Sass has two syntaxes. The new main syntax (as of Sass 3) is known as
“SCSS” (for “Sassy CSS”), and is a superset of CSS3’s syntax. This means
that every valid CSS3 stylesheet is valid SCSS as well. SCSS files use the
extension .scss.

The second, older syntax is known as the indented syntax (or just “Sass”).
Inspired by Haml’s terseness, it’s intended for people who prefer
conciseness over similarity to CSS. Instead of brackets and semicolons, it
uses the indentation of lines to specify blocks. Although no longer the
primary syntax, the indented syntax will continue to be supported. Files in
the indented syntax use the extension .sass.

Documentation
=============

For more information about Sass in general please visit the Sass homepage:
http://www.sass-lang.com/

For more information about the PHP port of Sass please visit the PhamlP
homepage;
http://code.google.com/p/phamlp/

Usage
=====

The extension hooks into the TYPO3 page rendering and converts all included
stylesheets ending with either .scss or .sass file extension into standard CSS.

The conversion is transparent, if a file can't be converted or is considered plain
CSS is it included without any modifications.

Dependencies
============

TYPO3 4.4, no previous versions have been tested although official hooks have been used,
so it should work with earlier releases of TYPO3.

Maintainers
===========

The mojo_sass extensions is maintained by Morton Jonuschat <yabawock@gmail.com>.
The PHP port of Haml and Sass is maintained by Chris L. Yates and contributors.

Project Info
============

mojo_sass is hosted on GitHub: https://github.com/yabawock/mojo_sass where your
contributions, forkings, comments and feedback are greatly welcomed.

Credits
=======
The icons used by the extension has been created by "newmoon" for the Ultimate Gnome
Icon set. It has been released under the GPLv2 - for more great icons from this set
visit the homepage at http://code.google.com/p/ultimate-gnome/

Copyright ©2010 Morton Jonuschat, released under the GPLv2 license.
