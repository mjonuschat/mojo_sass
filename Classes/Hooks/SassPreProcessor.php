<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2010-2011 Morton Jonuschat <yabawock@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the mojo_sass TYPO3 extension. The
 *  mojo_sass extension is free software; you can redistribute
 *  it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation;
 *  either version 2 of the License, or (at your option) any
 *  later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices
 *  to the license from the author is found in LICENSE.txt
 *  distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

require_once(t3lib_extMgm::extPath('mojo_sass', 'Vendor/Plugins/SassParser.php'));
class Tx_MojoSass_Hooks_SassPreProcessor {
  /**
   * Instance variable that contains the PHamlP SaSS Parser
   */
  protected $parser;

  /**
   * Constructor for the SassParser Hook
   */
  public function __construct() {
    $typo3DefaultOptions = array(
      'cache_location' => PATH_site . 'typo3temp' . DIRECTORY_SEPARATOR . 'sass-cache_default',
      'css_location' => PATH_site . 'typo3temp' . DIRECTORY_SEPARATOR . 'stylesheets/compiled_default',
      'load_paths' => array(
        PATH_site . 'fileadmin/templates',
      ),
      'style' => SassRenderer::STYLE_NESTED,
      'syntax' => SassFile::SCSS,
      'settings' => array()
    );
    if(is_array($GLOBALS['TSFE']->tmpl->setup['plugin.']['mojo_sass.']['settings.'])) {
      $typo3DefaultOptions['settings'] = array_merge(array(), $GLOBALS['TSFE']->tmpl->setup['plugin.']['mojo_sass.']['settings.']);
    }
    $options = array_merge($typo3DefaultOptions, $this->getExtensionConfiguration());
    $this->parser = t3lib_div::makeInstance('SassParser', $options);
  }

  /**
   * Inspects the included CSS files and converts SASS/SCSS to plain CSS where appropriate
   *
   * @param array $params
   * @param t3lib_PageRenderer $pageRenderer
   * @return void
   */
  public function convertToCss(&$params, t3lib_PageRenderer &$pageRenderer) {
    $cssFiles = is_array($params['cssFiles']) ? $params['cssFiles'] : array();
    $parsedCssFiles = array();
    foreach ($cssFiles as $file => $properties) {
      $syntax = $this->getFileSyntax($file);
      switch ($syntax) {
      case SassFile::SCSS:
      case SassFile::SASS:
        $file = $this->convertStylesheet($file);
        $properties['compress'] = 1;
        $properties['file'] = $file;
        break;
      }
      $parsedCssFiles[$file] = $properties;
    }
    $params['cssFiles'] = $parsedCssFiles;
  }

  /**
   * Convert a CSS file
   *
   * @param string  $filename   Source filename, relative to requested page
   * @return  string    Compressed filename, relative to requested page
   */
  public function convertStylesheet($filename) {
    $filenameAbsolute = PATH_site . $this->getFilenameFromSiteDir($filename);
    $unique = $filenameAbsolute . filemtime($filenameAbsolute) . filesize($filenameAbsolute);

    $pathinfo = pathinfo($filenameAbsolute);
    $targetFile = $this->targetDirectory . $pathinfo['filename'] . '-' . t3lib_div::shortMD5($unique) . '.css';

    if (!file_exists(PATH_site . $targetFile) || $this->getCssCacheStatus() === FALSE) {
      $contents = $this->parser->toCss($this->getFilenameFromSiteDir($filename));
      $this->writeFile($targetFile, $contents);
    }
    return $this->getFilenameFromSiteDir($targetFile);
  }

  /**
   * Try to smart guess the filetype by extension
   *
   * @param  string $file
   * @return string
   */
  protected function getFileSyntax($file) {
    return substr($file, -4);
  }

  /**
   * Writes $contents into file $filename together with a gzipped version into $filename.gz
   *
   * @param string  $filename   Target filename
   * @param strings $contents   File contents
   * @return  void
   */
  protected function writeFile($filename, $contents) {
    // write uncompressed file
    t3lib_div::writeFile(PATH_site . $filename, $contents);
  }

  /**
   * Finds the relative path to a file, relative to the TYPO3_mainDir.
   *
   * @param string $filename the name of the file
   * @return string the path to the file relative to the TYPO3_mainDir
   */
  private function getFilenameFromSiteDir($filename, $allowDirs = FALSE) {
    $baseUri = preg_quote(preg_replace('#^https?:#','', t3lib_div::getIndpEnv('TYPO3_SITE_URL')));
    $baseDir = preg_quote(PATH_site);

    $file = preg_replace("#^($baseUri|$baseDir)#", '', $filename);
    if(is_file(PATH_site . $file) || ($allowDirs && is_dir(PATH_site . $file))) {
      $filename = $file;
    }
    return $filename;
  }

  /**
   * Convert the extension configuration set in the extension manager into an
   * array of options suitable for PHamlP Sass Parser
   *
   * @return array
   */
  protected function getExtensionConfiguration() {
    $conf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['mojo_sass']);

    $options = array(
      'cache' => intval(!!$conf['enable.']['sass_cache']),
      'debug_info' => intval(!!$conf['enable.']['debug_info']),
      'line_numbers' => intval(!!$conf['enable.']['line_numbers']),
      'quiet' => intval(!!$conf['enable.']['quiet']),
      'style' => $conf['enable.']['style']
    );

    $tempPath = t3lib_div::resolveBackPath(PATH_site . $conf['enable.']['cache_location']);
    if(substr($tempPath,-1) !== '/') {
      $tempPath .= '/';
    }
    if (t3lib_div::isAllowedAbsPath($tempPath)) {
      $options['cache_location'] = $tempPath;
    }

    $tempPath = t3lib_div::resolveBackPath(PATH_site . $conf['enable.']['css_location']);
    if(substr($tempPath,-1) !== '/') {
      $tempPath .= '/';
    }
    if (t3lib_div::isAllowedAbsPath($tempPath)) {
      $options['css_location'] = $tempPath;
      $this->targetDirectory = $this->getFilenameFromSiteDir($tempPath, TRUE);
    }
    return $options;
  }
  /**
   * Check CSS Cache status
   */
  protected function getCssCacheStatus() {
    $conf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['mojo_sass']);
    return !!$conf['enable.']['css_cache'];
  }
}
