<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2010 Morton Jonuschat <yabawock@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the mojo_sass TYPO3 extension. The
 *  mojo_sass extension is free software; you can redistribute
 *  it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation;
 *  either version 2 of the License, or (at your option) any
 *  later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices
 *  to the license from the author is found in LICENSE.txt
 *  distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

require_once(PATH_typo3.'interfaces/interface.backend_cacheActionsHook.php');

class Tx_MojoSass_Hooks_CacheMenu implements backend_cacheActionsHook {

  /**
   * Constructor for the CacheMenu Hooks
   */
  public function __construct() {
  }

  /**
   * Add clear SaSS and CSS Cache menu entry
   * @param array $a_cacheActions
   * @param array $a_optionValues
   * @return void
   * @see typo3/interfaces/backend_cacheActionsHook#manipulateCacheActions($cacheActions, $optionValues)
   */
  public function manipulateCacheActions(&$a_cacheActions, &$a_optionValues) {
    if(($GLOBALS['BE_USER']->isAdmin() || $GLOBALS['BE_USER']->getTSConfigVal('options.clearCache.sass_cache')) && $GLOBALS['TYPO3_CONF_VARS']['EXT']['extCache']) {
      $s_title = $GLOBALS['LANG']->sL('LLL:EXT:mojo_sass/Resources/Private/Language/locallang.xml:backend.label.clear_cache', true);
      $s_imagePath = t3lib_extMgm::extRelPath('mojo_sass').'Resources/Public/Icons/';
      //if(strpos($s_imagePath,'typo3conf') !== false) $s_imagePath = '../'.$s_imagePath;
      $a_cacheActions[] = array(
        'id'    => 'sass_cache',
        'title' => $s_title,
        'href' => 'ajax.php?ajaxID=mojo_sass::purge',
        'icon'  => '<img src="'.$s_imagePath.'be_icon.gif" title="'.$s_title.'" alt="'.$s_title.'" />',
      );
      $a_optionValues[] = 'clearCacheSassCache';
    }
  }

  /**
   * Clear SaSS and CSS Cache
   *
   * @return void
   */
  public static function clearCacheFiles() {
    $conf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['mojo_sass']);
    $sassCachePath = t3lib_div::resolveBackPath(PATH_site . $conf['enable.']['cache_location']);
    self::removeFilesInDir($sassCachePath, 'sassc');
    $cssCachePath = t3lib_div::resolveBackPath(PATH_site . $conf['enable.']['css_location']);
    self::removeFilesInDir($cssCachePath, 'css');
  }

  /**
   * Perform removal of files in a directory
   * @param string $path
   * @return void
   */
  protected static function removeFilesInDir($path, $ext='sassc') {
    if (@is_dir($path)) {
      $filesInDir = t3lib_div::getFilesInDir($path, $ext, 1);

      foreach ($filesInDir as $kk => $vv) {
        unlink($vv);
      }
    }

  }
}

/**
 * XCLASS Inclusion
 */
if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/mojo_sass/Classes/Hooks/CacheMenu.php'])    {
  include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/mojo_sass/Classes/Hooks/CacheMenu.php']);
}
